import os
import pymongo
from pydantic import BaseModel
from typing import Optional
from pymongo.errors import DuplicateKeyError

client = pymongo.MongoClient(os.environ["DATABASE_URL"])
dbname = os.environ["DATABASE_NAME"]


class DuplicateAccountError(ValueError):
    pass


class User(BaseModel):
    id: str
    email: str
    password: str
    full_name: str
    role: str
    address: str
    phone_number: str
    operating_hours: Optional[str]
    dl_num: Optional[str]
    plate_num: Optional[str]
    pickup_instructions: Optional[str]
    dropoff_instructions: Optional[str]


class UserIn(BaseModel):
    email: str
    password: str
    full_name: str
    role: str
    address: str
    phone_number: str
    operating_hours: Optional[str]
    dl_num: Optional[str]
    plate_num: Optional[str]
    pickup_instructions: Optional[str]
    dropoff_instructions: Optional[str]


class UserOut(BaseModel):
    id: str
    email: str
    full_name: str
    role: str
    address: str
    phone_number: str
    operating_hours: Optional[str]
    dl_num: Optional[str]
    plate_num: Optional[str]
    pickup_instructions: Optional[str]
    dropoff_instructions: Optional[str]


class UserUpdate(BaseModel):
    full_name: Optional[str]
    address: Optional[str]
    phone_number: Optional[str]
    operating_hours: Optional[str]
    dl_num: Optional[str]
    plate_num: Optional[str]
    pickup_instructions: Optional[str]
    dropoff_instructions: Optional[str]


class UserRepo:
    def get(self, email) -> User:
        db = client[dbname]
        result = db.users.find_one({"email": email})
        if not result:
            return None
        result["id"] = str(result["_id"])
        return User(**result)

    def create(self, user: UserIn, hashed_password: str) -> User:
        db = client[dbname]
        data = user.dict()
        data["password"] = hashed_password
        try:
            db.users.insert_one(data)
        except DuplicateKeyError:
            raise DuplicateAccountError()
        return self.get(user.email)

    def update(self, email, data):
        db = client[dbname]
        result = db.users.update_one(
            {"email": email}, {"$set": data.dict(exclude_unset=True)}
        )
        if not result:
            return None
        return self.get(email)
