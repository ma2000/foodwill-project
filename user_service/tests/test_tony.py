from fastapi.testclient import TestClient
from main import app

client = TestClient(app)


def test_if_user_is_logged_in():

    response = client.get("/token")

    assert response.status_code == 200
    assert response.json() is None
