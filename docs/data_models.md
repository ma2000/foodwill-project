### Pydantic Models
User microservice  

| **User microservice**  | **type** | **optional**     |
|------------------------|----------|-----------|
| class User(BaseModel): | type     | optional  |
| id:                    | str      | no        |
| email:                 | str      | no        |
| password:              | str      | no        |
| full_name:             | str      | no        |
| role:                  | str      | no        |
| address:               | str      | no        |
| phone_number:          | str      | no        |
| operating_hours:       | str      | yes       |  


| **Donation microservice**          | **type** |       **optional**
|------------------------------------|----------|--------------------------|
| class DonationIn(BaseModel):       | type     | optional                 |
| quantity: int                      | int      | no                       |
| value: int                         | int      | no                       |
| ready_time: datetime               | datetime | no                       |
| status: str = "UNCLAIMED"          | str      | no: default="UNCLAIMED"  |
| volunteer_id: Optional[str] = None | str      | yes                      |
| receiver_id: Optional[str] = None  | str      | yes                      |
| provider_id: Optional[str] = None  | str      | yes                      |
| active: bool = True                | boolean  | no: default=True         |
| prov_name: Optional[str] = None    | str      | yes                      |
| prov_address: Optional[str] = None | str      | yes                      |
| prov_phone: Optional[str] = None   | str      | yes                      |
| rec_name: Optional[str] = None     | str      | yes                      |
| rec_address: Optional[str] = None  | str      | yes                      |
| rec_phone: Optional[str] = None    | str      | yes                      |
| vol_name: Optional[str] = None     | str      | yes                      |
| vol_phone: Optional[str] = None    | str      | yes                      |

