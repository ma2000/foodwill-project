


## **API Design**
## Microservices
___
### User
### Donations
___

## API Endpoints

### Donations Microservice

#### Create new donation
* Endpoint path: /api/donations
* Endpoint method: POST

* Headers:
  * Authorization: Bearer token

* Request shape (JSON):
    ```
    {
      "quantity": 0,
      "value": 0,
      "ready_time": "2022-12-09T20:38:11.506Z",
      "status": "UNCLAIMED",
      "volunteer_id": "string",
      "receiver_id": "string",
      "provider_id": "string",
      "active": true,
      "prov_name": "string",
      "prov_address": "string",
      "prov_phone": "string",
      "rec_name": "string",
      "rec_address": "string",
      "rec_phone": "string",
      "vol_name": "string",
      "vol_phone": "string",
      "dropoff_instructions": "string",
      "pickup_instructions": "string"
    }
    ```

* Response: Donation created
* Response shape (JSON):
    ```
    {
        response shape same as request shape
    }
    ```
#### Get all donations

* Endpoint path: /api/donations
* Endpoint method: GET

* Headers:
  * Authorization: Bearer token

* Response: Get All Donations
* Response shape (JSON):
    ```
    {
      "donations": [
        {
          "id": "string",
          "quantity": 0,
          "value": 0,
          "pickup_instructions": "string",
          "ready_time": "2022-12-09T20:39:46.019Z",
          "status": "string",
          "volunteer_id": "string",
          "receiver_id": "string",
          "provider_id": "string",
          "active": true,
          "prov_name": "string",
          "prov_address": "string",
          "prov_phone": "string",
          "rec_name": "string",
          "rec_address": "string",
          "rec_phone": "string",
          "vol_name": "string",
          "vol_phone": "string",
          "dropoff_instructions": "string"
        }
      ]
    }
    ```
#### Get user donations

* Endpoint path: /api/donations/user/{user_id}
* Endpoint method: GET

* Headers:
  * Authorization: Bearer token

* Response: Get User Donation
* Response shape (JSON):
    ```
    {
      "donations": [
        {
          "id": "string",
          "quantity": 0,
          "value": 0,
          "pickup_instructions": "string",
          "ready_time": "2022-12-09T20:41:29.123Z",
          "status": "string",
          "volunteer_id": "string",
          "receiver_id": "string",
          "provider_id": "string",
          "active": true,
          "prov_name": "string",
          "prov_address": "string",
          "prov_phone": "string",
          "rec_name": "string",
          "rec_address": "string",
          "rec_phone": "string",
          "vol_name": "string",
          "vol_phone": "string",
          "dropoff_instructions": "string"
        }
      ]
    }
    ```

#### Get active user donations

* Endpoint path: /api/donations/active/user/{user_id}
* Endpoint method: GET

* Headers:
  * Authorization: Bearer token

* Response: Get Active User Donation
* Response shape (JSON):
    ```
    {
      "donations": [
        {
          "id": "string",
          "quantity": 0,
          "value": 0,
          "pickup_instructions": "string",
          "ready_time": "2022-12-09T20:41:29.123Z",
          "status": "string",
          "volunteer_id": "string",
          "receiver_id": "string",
          "provider_id": "string",
          "active": true,
          "prov_name": "string",
          "prov_address": "string",
          "prov_phone": "string",
          "rec_name": "string",
          "rec_address": "string",
          "rec_phone": "string",
          "vol_name": "string",
          "vol_phone": "string",
          "dropoff_instructions": "string"
        }
      ]
    }
    ```
#### Get donation detail

* Endpoint path: /api/donations/{donation_id}
* Endpoint method: GET

* Headers:
  * Authorization: Bearer token

* Response: Get Donation Detail
* Response shape (JSON):
    ```
    {
      "donations": [
        {
          "id": "string",
          "quantity": 0,
          "value": 0,
          "pickup_instructions": "string",
          "ready_time": "2022-12-09T20:41:29.123Z",
          "status": "string",
          "volunteer_id": "string",
          "receiver_id": "string",
          "provider_id": "string",
          "active": true,
          "prov_name": "string",
          "prov_address": "string",
          "prov_phone": "string",
          "rec_name": "string",
          "rec_address": "string",
          "rec_phone": "string",
          "vol_name": "string",
          "vol_phone": "string",
          "dropoff_instructions": "string"
        }
      ]
    }
    ```

 #### Update donation
* Endpoint path: /api/donations/{donation_id}
* Endpoint method: PUT

* Headers:
  * Authorization: Bearer token

* Request shape (JSON):
    ```
    {
      "quantity": 0,
      "value": 0,
      "pickup_instructions": "string",
      "ready_time": "2022-12-09T20:46:35.114Z",
      "status": "string",
      "volunteer_id": "string",
      "receiver_id": "string",
      "provider_id": "string",
      "active": true,
      "prov_name": "string",
      "prov_address": "string",
      "prov_phone": "string",
      "rec_name": "string",
      "rec_address": "string",
      "rec_phone": "string",
      "vol_name": "string",
      "vol_phone": "string",
      "dropoff_instructions": "string"
    }
    ```

* Response: Donation created
* Response shape (JSON):
    ```
    {
        same as request shape with changes
    }
    ```

  #### Delete donation
* Endpoint path: /api/donations/{donation_id}
* Endpoint method: DELETE

* Headers:
  * Authorization: Bearer token

* Response: Donation created
* Response shape (JSON):
    ```
    {
        true
    }
    ```
___
### User microservice
#### *Log in*

* Endpoint path: /token
* Endpoint method: POST

* Request shape (form):
  * username: string
  * password: string

* Response: Account information and a token
* Response shape (JSON):
    ```json
    {
      "access_token": "string",
      "token_type": "Bearer"
    }
    ```

#### *Log out*


* Endpoint path: /token
* Endpoint method: DELETE

* Headers:
  * Authorization: Bearer token

* Response: Always true
* Response shape (JSON):
    ```json
    true
    ```
___
#### Create new user

* Endpoint path: /api/users
* Endpoint method: POST

* Headers:
  * Authorization: Bearer token

* Request shape (JSON):
    ```
    {
      "email": "string",
      "password": "string",
      "full_name": "string",
      "role": "string",
      "address": "string",
      "phone_number": "string",
      "operating_hours": "string",
      "dl_num": "string",
      "plate_num": "string",
      "pickup_instructions": "string",
      "dropoff_instructions": "string"
    }
    ```

* Response: User created
* Response shape (JSON):
    ```
    {
        same as request shape with data in fields
    }
    ```
#### Get Token

* Endpoint path: /token
* Endpoint method: GET

* Headers:
  * Authorization: Bearer token

* Response: Provider updated
* Response shape (JSON):
    ```
    {
      "access_token": "string",
      "token_type": "Bearer",
      "account": {
        "id": "string",
        "email": "string",
        "full_name": "string",
        "role": "string",
        "address": "string",
        "phone_number": "string",
        "operating_hours": "string",
        "dl_num": "string",
        "plate_num": "string",
        "pickup_instructions": "string",
        "dropoff_instructions": "string"
      }
    }
    ```

#### Update user

* Endpoint path: /api/users/
* Endpoint method: PUT

* Headers:
  * Authorization: Bearer token

* Request shape (JSON):
    ```
    {
      "full_name": "string",
      "address": "string",
      "phone_number": "string",
      "operating_hours": "string",
      "dl_num": "string",
      "plate_num": "string",
      "pickup_instructions": "string",
      "dropoff_instructions": "string"
    }
    ```

* Response: Provider updated
* Response shape (JSON):
    ```
    {
        same as response with fields changed
    }
    ```

___