## User GHI
**WARNING** The image files will take a little time to load, thanks for your patience!!  

**Landing Page**
___
When users arrive at our base url they will have the option to signup or login.
![landing page image](./images/landing_page.png)

**Signup Page**
___
Users will choose a role and upon submission be directed to a user-type dashboard.
![singup page image](./images/signup_page.png)

**User Dashboard**
___
The dashboard will display lists and options relevant to the users role. Each type of user will have slightly different application functionality. Providers are the only user that can create or cancel a donation instance. Only receivers can claim donations. Only volunteers can choose to deliver donations or cancel delivery. Changes to the donation instance occur in realtime with Redux Toolkit Query implementation managing state across both microservices.
![dashboard page image](./images/user_dashboard.png)
___

### Development wire frame:

**Wire frame**
![wireframe image](./images/Foodwill-initial-wireframe.png)