fastapi[all]>0.78.0
uvicorn[standard]==0.17.6
pytest
pymongo==4.1.1
pymongo[srv]
jwtdown-fastapi>=0.2.0
