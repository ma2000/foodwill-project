import os
import pymongo
from datetime import datetime
from pydantic import BaseModel
from typing import Optional
from bson.objectid import ObjectId

client = pymongo.MongoClient(os.environ["DATABASE_URL"])
dbname = os.environ["DATABASE_NAME"]


class DonationIn(BaseModel):
    quantity: int
    value: int
    ready_time: datetime
    status: str = "UNCLAIMED"
    volunteer_id: Optional[str] = None
    receiver_id: Optional[str] = None
    provider_id: Optional[str] = None
    active: bool = True
    prov_name: Optional[str] = None
    prov_address: Optional[str] = None
    prov_phone: Optional[str] = None
    rec_name: Optional[str] = None
    rec_address: Optional[str] = None
    rec_phone: Optional[str] = None
    vol_name: Optional[str] = None
    vol_phone: Optional[str] = None
    dropoff_instructions: Optional[str] = None
    pickup_instructions: str


class DonationUpdateIn(BaseModel):
    quantity: Optional[int]
    value: Optional[int]
    pickup_instructions: Optional[str]
    ready_time: Optional[datetime]
    status: Optional[str]
    volunteer_id: Optional[str]
    receiver_id: Optional[str]
    provider_id: Optional[str]
    active: Optional[bool]
    prov_name: Optional[str]
    prov_address: Optional[str]
    prov_phone: Optional[str]
    rec_name: Optional[str]
    rec_address: Optional[str]
    rec_phone: Optional[str]
    vol_name: Optional[str]
    vol_phone: Optional[str]
    dropoff_instructions: Optional[str]


class DonationOut(BaseModel):
    id: str
    quantity: int
    value: int
    pickup_instructions: str
    ready_time: datetime
    status: str
    volunteer_id: Optional[str]
    receiver_id: Optional[str]
    provider_id: Optional[str]
    active: bool
    prov_name: Optional[str]
    prov_address: Optional[str]
    prov_phone: Optional[str]
    rec_name: Optional[str]
    rec_address: Optional[str]
    rec_phone: Optional[str]
    vol_name: Optional[str]
    vol_phone: Optional[str]
    dropoff_instructions: Optional[str]


class AllDonationsOut(BaseModel):
    donations: list[DonationOut]


class DonationQueries:
    def get_all_donations(self, status):
        db = client[dbname]
        if status is not None:
            result = list(db.donations.find({"status": status}))
        elif status is None:
            result = list(db.donations.find())
        for value in result:
            value["id"] = str(value["_id"])
        return result

    def get_donation(self, id):
        db = client[dbname]
        result = db.donations.find_one({"_id": ObjectId(id)})
        if result:
            result["id"] = str(result["_id"])
        return result

    def get_user_donation(self, id):
        db = client[dbname]
        result = list(
            db.donations.find(
                {
                    "$or": [
                        {"provider_id": id},
                        {"volunteer_id": id},
                        {"receiver_id": id},
                    ],
                }
            )
        )
        for value in result:
            value["id"] = str(value["_id"])
        return result

    def get_active_user_donation(self, id):
        db = client[dbname]
        result = list(
            db.donations.find(
                {
                    "active": True,
                    "$or": [
                        {"provider_id": id},
                        {"volunteer_id": id},
                        {"receiver_id": id},
                    ],
                }
            )
        )
        for value in result:
            value["id"] = str(value["_id"])
        return result

    def create_donation(self, data):
        db = client[dbname]
        result = db.donations.insert_one(data.dict())
        if result.inserted_id:
            result = self.get_donation(result.inserted_id)
        return result

    def update_donation(self, id, data):
        db = client[dbname]
        result = db.donations.update_one(
            {"_id": ObjectId(id)}, {"$set": data.dict(exclude_unset=True)}
        )
        if result:
            result = self.get_donation(id)
        return result

    def delete_donation(self, id):
        db = client[dbname]
        result = db.donations.delete_one({"_id": ObjectId(id)})
        return result.deleted_count > 0
