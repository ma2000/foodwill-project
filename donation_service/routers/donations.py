from fastapi import APIRouter, Depends
from typing import Union
from other_auth import authenticator

from queries.donations import (
    DonationQueries,
    DonationIn,
    DonationOut,
    AllDonationsOut,
    DonationUpdateIn,
)

router = APIRouter()


@router.post("/api/donations/", response_model=DonationOut)
def create_donation(
    donation: DonationIn,
    repo: DonationQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.create_donation(donation)


@router.get("/api/donations/", response_model=AllDonationsOut)
def get_all_donations(
    status: Union[str, None] = None,
    repo: DonationQueries = Depends(),
):
    return {
        "donations": repo.get_all_donations(status),
    }


@router.get("/api/donations/user/{user_id}", response_model=AllDonationsOut)
def get_user_donation(
    user_id: str,
    queries: DonationQueries = Depends(),
):
    return {"donations": queries.get_user_donation(user_id)}


@router.get(
    "/api/donations/active/user/{user_id}", response_model=AllDonationsOut
)
def get_active_user_donation(
    user_id: str,
    queries: DonationQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return {"donations": queries.get_active_user_donation(user_id)}


@router.get(
    "/api/donations/{donation_id}", response_model=Union[DonationOut, None]
)
def get_donation(
    donation_id: str,
    queries: DonationQueries = Depends(),
):
    return queries.get_donation(donation_id)


@router.put("/api/donations/{donation_id}", response_model=DonationOut)
def update_donation(
    donation_id: str,
    donation: DonationUpdateIn,
    repo: DonationQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.update_donation(donation_id, donation)


@router.delete("/api/donations/{donation_id}", response_model=bool)
def delete_donation(
    donation_id: str,
    repo: DonationQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.delete_donation(donation_id)
