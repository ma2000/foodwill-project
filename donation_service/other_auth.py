import os
from jwtdown_fastapi.authentication import Authenticator


class Auth(Authenticator):
    async def get_account_data(self, username: str, accounts):
        pass

    def get_account_getter(self, accounts):
        pass

    def get_hashed_password(self, account) -> str:
        return account.password

    def get_account_data_for_cookie(self, account):
        return account.email, account.dict()


authenticator = Auth(os.environ["SIGNING_KEY"])
