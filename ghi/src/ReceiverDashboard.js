import React from "react";
import { useNavigate } from "react-router-dom";
import ReceiverMain from "./ReceiverMain";

export default function ReceiverDashBoard(props) {
  const navigate = useNavigate();

  return (
    <>
      <div className="container text-center">
        <div
          style={{ width: "650px" }}
          className="row border border-secondary border-3 justify-content-center p-2 mt-4 rounded mx-auto text-muted"
        >
          <div style={{}} className="col">
            <h1 className="">Receiver Dashboard</h1>
          </div>
        </div>
      </div>
      <div className=" mt-5">
        <ReceiverMain userData={props.userData} />
      </div>
      <div className="container text-center">
        <div className="row justify-content-center p-2 mt-4 mb-4 mx-auto">
          <div style={{}} className="col">
            <button
              type="button"
              onClick={() => {
                navigate("/donations/history");
              }}
              className="btn btn-dark me-4"
            >
              Donations History
            </button>
            <button
              type="button"
              onClick={() => {
                navigate("/receiver/update");
              }}
              className="btn btn-dark me-4"
            >
              Update My Info
            </button>
          </div>
        </div>
      </div>
    </>
  );
}
