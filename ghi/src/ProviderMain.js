import React from "react";
import ActiveProviderDonationsList from "./ProviderActiveDonations";

export default function ProviderMain(props) {
  return (
    <>
      <div>
        <ActiveProviderDonationsList userData={props.userData} />
      </div>
    </>
  );
}
