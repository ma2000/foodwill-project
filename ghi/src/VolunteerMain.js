import React from "react";
import ClaimedDonationsList from "./ClaimedDonationsList";
import VolActiveDonations from "./VolActiveDonations";

export default function VolunteerMain(props) {
  return (
    <>
      <div>
        <ClaimedDonationsList />
      </div>
      <div>
        <VolActiveDonations userData={props.userData} />
      </div>
    </>
  );
}
