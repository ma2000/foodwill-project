import React from "react";
import { useNavigate } from "react-router-dom";
import ProviderMain from "./ProviderMain";

export default function ProviderDashBoard(props) {
  const navigate = useNavigate();

  return (
    <>
      <div className="container text-center">
        <div
          style={{ width: "650px" }}
          className="row border border-secondary border-3 justify-content-center p-2 mt-4 rounded mx-auto text-muted"
        >
          <div style={{}} className="col">
            <h1 className="">Provider Dashboard</h1>
          </div>
        </div>
        <div className="p-4">
          <button
            type="button"
            onClick={() => {
              navigate("/donations/new");
            }}
            className="btn btn-dark me-4"
          >
            New Donation
          </button>
        </div>
      </div>
      <div className=" mt-5">
        <ProviderMain userData={props.userData} />
      </div>
      <div className="container text-center">
        <div className="row justify-content-center p-2 mt-4 mb-4 mx-auto">
          <div style={{}} className="col">
            <button
              type="button"
              onClick={() => {
                navigate("/donations/history");
              }}
              className="btn btn-dark me-4"
            >
              Donations History
            </button>
            <button
              type="button"
              onClick={() => {
                navigate("/provider/update");
              }}
              className="btn btn-dark me-4"
            >
              Update My Info
            </button>
          </div>
        </div>
      </div>
    </>
  );
}
