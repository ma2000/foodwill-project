import { configureStore } from "@reduxjs/toolkit";
import { setupListeners } from "@reduxjs/toolkit/query";

import { donationApi } from "./donationApi";
import { userApi } from "./userApi";

export const store = configureStore({
  reducer: {
    [donationApi.reducerPath]: donationApi.reducer,
    [userApi.reducerPath]: userApi.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware()
      .concat(donationApi.middleware)
      .concat(userApi.middleware),
});

setupListeners(store.dispatch);
