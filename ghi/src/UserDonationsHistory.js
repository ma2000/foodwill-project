import React from "react";
import { useNavigate } from "react-router-dom";
import { useGetDonationByUserQuery } from "./store/donationApi";

export default function UserDonationsHistory(props) {
  const navigate = useNavigate();
  const { data: donationData, isLoading } = useGetDonationByUserQuery(
    props.userData.account.id
  );

  if (isLoading) {
    return <progress className="progress is-primary" max="100"></progress>;
  }

  return (
    <>
      <h1 className="text-center p-4">My Donations History</h1>
      <div className="table-responsive">
        <table className="table table-hover table-striped mx-3 align-middle">
          <thead>
            <tr>
              <th></th>
              <th>Active</th>
              <th>Status</th>
              <th>Value ($)</th>
              <th>Volunteer</th>
              <th>Provider</th>
              <th>Receiver</th>
            </tr>
          </thead>
          <tbody>
            {donationData.donations.map((donation) => {
              return (
                <tr key={donation.id}>
                  <td>
                    <button
                      className="btn btn-outline-dark"
                      onClick={() => navigate(`/donations/${donation.id}`)}
                    >
                      {" "}
                      DETAILS{" "}
                    </button>
                  </td>
                  <td>{String(donation.active)}</td>
                  <td>{donation.status}</td>
                  <td>{donation.value}</td>
                  <td>{donation.vol_name}</td>
                  <td>{donation.prov_name}</td>
                  <td>{donation.rec_name}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
}
