import React from "react";
import { useState } from "react";
import { useLogInMutation } from "./store/userApi";
import { useNavigate } from "react-router-dom";

export default function DonationNew() {
  const navigate = useNavigate();
  const [logIn, result] = useLogInMutation();
  const [formInput, setFormInput] = useState({
    email: "",
    password: "",
  });

  const handleInputChange = (e) => {
    setFormInput({
      ...formInput,
      [e.target.id]: e.target.value,
    });
  };

  const clearState = () => {
    setFormInput({
      email: "",
      password: "",
    });
  };

  const handleFormSubmit = async (e) => {
    e.preventDefault();
    let data = { ...formInput };
    logIn(data)
      .unwrap()
      .then((payload) => {})
      .catch((error) => console.error("rejected", error));
    clearState();
  };

  if (result.isSuccess) {
    navigate("/dashboard");
  }

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <div className="text-center mb-4">
              <h1>Log In</h1>
            </div>
            <form onSubmit={handleFormSubmit} id="login-form">
              <div className="form-floating mb-3">
                <input
                  value={formInput.email}
                  onChange={handleInputChange}
                  placeholder="Email"
                  required
                  type="email"
                  id="email"
                  name="email"
                  className="form-control"
                />
                <label htmlFor="email">Email</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={formInput.password}
                  onChange={handleInputChange}
                  placeholder="Password"
                  required
                  type="password"
                  id="password"
                  name="password"
                  className="form-control"
                />
                <label htmlFor="password">Password</label>
              </div>
              <div className="text-end">
                <button className="btn btn-outline-dark">LOG IN</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}
