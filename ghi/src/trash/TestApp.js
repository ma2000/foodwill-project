import { useEffect, useState } from "react";
import Construct from "./Construct.js";
import ErrorNotification from "./ErrorNotification";
import "./App.css";
import LoginForm from "./LoginForm";
import DonationsList from "./TestDonations.js";
import DonationNew from "./TestDonationNew.js";
import MainPage from "./MainPage.js";
import ProviderUpdate from "./ProviderUpdateForm.js";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Nav from "./TonyNav";
import SignUp from "./SignupForm.js";
import VolunteerMain from "./VolunteerMain.js";
import DashBoard from "./DashBoard.js";
import { useGetTokenQuery } from "./store/userApi.js";
import VolunteerUpdate from "./volunteerUpdateForm.js";
import UserDonationsHistory from "./UserDonationsHistory.js";
import { DonationDetail } from "./DonationDetail.js";
import ProviderDashBoard from "./ProviderDash.js";

function App() {
  const { data: userData, isLoading } = useGetTokenQuery();

  if (isLoading) {
    return <progress className="progress is-primary" max="100"></progress>;
  }

  if (userData) {
    // console.log("APP---", userData);
    return (
      <BrowserRouter>
        <Nav />
        <Routes>
          <Route path="/" element={<MainPage />}></Route>
          <Route path="list" element={<DonationsList />}></Route>
          <Route path="new" element={<DonationNew />}></Route>
          <Route
            path="dashboard"
            element={<DashBoard userData={userData} />}
          ></Route>
          <Route
            path="userupdate"
            element={<VolunteerUpdate userData={userData} />}
          ></Route>
          <Route
            path="history"
            element={<UserDonationsHistory userData={userData} />}
          ></Route>
          <Route path="donations/:id" element={<DonationDetail />}></Route>
          <Route
            path="providerupdate"
            element={<ProviderUpdate userData={userData} />}
          ></Route>
          <Route
            path="providerdashboard"
            element={<ProviderDashBoard userData={userData} />}
          ></Route>
        </Routes>
      </BrowserRouter>
    );
  } else {
    return (
      <BrowserRouter>
        <Nav />
        <Routes>
          <Route path="/" element={<MainPage />}></Route>
          <Route path="login" element={<LoginForm />}></Route>
          <Route path="signup" element={<SignUp />}></Route>
        </Routes>
      </BrowserRouter>
    );
  }
}
export default App;
