import { BrowserRouter, Routes, Route } from "react-router-dom";
import React, { useEffect, useState } from "react";
import AllDonationsList from "./AllDonationsList";
import MainPage from "./MainPage";
import VolunteerUpdate from "./volunteerUpdateForm";
import LoginForm from "./LoginForm";
import ReceiverUpdate from "./ReceiverUpdateForm";

// import Nav from "./ReceiverNav";
import DonationsList from "./TestDonations.js";
import DonationNew from "./TestDonationNew.js";
import { useGetTokenQuery } from "./store/userApi";
import Nav from "./TonyNav";
import UnclaimedDonationsList from "./UnclaimedDonationsList";

function App() {
  const { data: userData, isLoading } = useGetTokenQuery();

  if (userData) {
    // setDefaultUserData(userData)
    return (
      <BrowserRouter>
        <div>
          <Routes>
            <Route path="/" element={<MainPage />} />
            {/* <Route path="alldonations" element={<AllDonationsList allDonations={allDonations}/>} /> */}
            <Route
              path="volunteerupdate"
              element={<VolunteerUpdate userData={userData} />}
            ></Route>
            <Route path="login" element={<LoginForm />}></Route>
            <Route path="list" element={<DonationsList />}></Route>
            <Route path="new" element={<DonationNew />}></Route>

            <Route path="receiverlist" element={<UnclaimedDonationsList />} />

            <Route
              path="receiverupdateform"
              element={<ReceiverUpdate userData={userData} />}
            />
          </Routes>
        </div>
      </BrowserRouter>
    );
  }

  return <h1>Still loading</h1>;
}

export default App;
