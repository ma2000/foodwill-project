import React from "react";
import { useState } from "react";
import {
  useGetDonationsQuery,
  useUpdateDonationMutation,
} from "./store/donationApi";
import { useGetTokenQuery } from "./store/userApi";
import { Outlet } from "react-router-dom";

export default function DonationsList() {
  const { data: donationData, error, isLoading } = useGetDonationsQuery();
  const { data: userData } = useGetTokenQuery();
  const [updateDonation, result] = useUpdateDonationMutation();

  if (isLoading) {
    return <progress className="progress is-primary" max="100"></progress>;
  }

  function handleClaim(id) {
    let payload = {
      status: "CLAIMED",
      receiver_id: userData.account.id,
    };
    let body = {
      data: payload,
      id: id,
    };
    updateDonation(body);
  }

  const getDateTime = (dateTime) => {
    const date = new Date(dateTime);
    return date.toLocaleString();
  };

  if (!userData) {
    return <h2>Please Log In for more functions</h2>;
  }
  // console.log("DATA::::", donationData)

  return (
    <>
      <h1>Donations Lists</h1>
      <table className="table table-hover table-striped">
        <thead>
          <tr>
            <th></th>
            <th>ID</th>
            <th>Active</th>
            <th>Status</th>
            <th>Quantity</th>
            <th>Value</th>
            <th>Deli Instruc</th>
            <th>Ready time</th>
            <th>volun id</th>
            <th>provider id</th>
            <th>receiver id</th>
          </tr>
        </thead>
        <tbody>
          {donationData.donations.map((donation) => {
            return (
              <tr key={donation.id}>
                <td>
                  <button
                    className="btn btn-success"
                    onClick={() => handleClaim(donation.id)}
                  >
                    {" "}
                    CLAIM{" "}
                  </button>
                </td>
                <td>{donation.id}</td>
                <td>{String(donation.active)}</td>
                <td>{donation.status}</td>
                <td>{donation.quantity}</td>
                <td>{donation.value}</td>
                <td>{donation.delivery_instructions}</td>
                <td>{getDateTime(donation.ready_time)}</td>
                <td>{donation.volunteer_id}</td>
                <td>{donation.provider_id}</td>
                <td>{donation.receiver_id}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}
