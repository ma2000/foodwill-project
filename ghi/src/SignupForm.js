import React from "react";
import { useState } from "react";
import { useSignUpMutation } from "./store/userApi";
import { useNavigate } from "react-router-dom";

export default function SignUp() {
  const navigate = useNavigate();
  const [signUp] = useSignUpMutation();
  const [role, setRole] = useState("");
  const [formInput, setFormInput] = useState({
    email: "",
    password: "",
    full_name: "",
    address: "",
    phone_number: "",
    operating_hours: "",
    dl_num: "",
    plate_num: "",
    dropoff_instructions: "",
    pickup_instructions: "",
  });

  const handleRoleChange = (e) => {
    setRole(e.target.value);
  };

  const handleInputChange = (e) => {
    setFormInput({
      ...formInput,
      [e.target.id]: e.target.value,
    });
  };

  const clearState = () => {
    setFormInput({
      email: "",
      password: "",
      full_name: "",
      address: "",
      phone_number: "",
      operating_hours: "",
      dl_num: "",
      plate_num: "",
      dropoff_instructions: "",
      pickup_instructions: "",
    });
  };

  const handleFormSubmit = async (e) => {
    e.preventDefault();
    let data = { ...formInput };
    data["role"] = role;
    signUp(data)
      .unwrap()
      .then((payload) => {
        navigate("/dashboard");
      })
      .catch((error) => console.error("rejected", error));
    clearState();
  };

  let dropdownClass = "form-select";
  let commonInputClass = "form-floating mb-3 d-none";
  let operatingClass = "form-floating mb-3 d-none";
  let providerInputClass = "form-floating mb-3 d-none";
  let receiverInputClass = "form-floating mb-3 d-none";
  let volunteerInputClass = "form-floating mb-3 d-none";
  if (role) {
    if (role === "provider") {
      commonInputClass = "form-floating mb-3";
      operatingClass = "form-floating mb-3";
      providerInputClass = "form-floating mb-3";
    } else if (role === "receiver") {
      commonInputClass = "form-floating mb-3";
      operatingClass = "form-floating mb-3";
      receiverInputClass = "form-floating mb-3";
    } else {
      commonInputClass = "form-floating mb-3";
      volunteerInputClass = "form-floating mb-3";
    }
  }

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <div className="text-center mb-4">
              <h1>Sign Up</h1>
            </div>
            <form onSubmit={handleFormSubmit} id="create-user-form">
              <div className="form-floating mb-3">
                <select
                  value={role}
                  onChange={handleRoleChange}
                  name="role"
                  id="role"
                  className={dropdownClass}
                  required
                >
                  <option value="">Choose a Role</option>
                  <option value="provider">Provider</option>
                  <option value="receiver">Receiver</option>
                  <option value="volunteer">Volunteer</option>
                </select>
              </div>
              <div className={commonInputClass}>
                <input
                  value={formInput.email}
                  onChange={handleInputChange}
                  placeholder="Email"
                  required
                  type="email"
                  id="email"
                  name="email"
                  className="form-control"
                />
                <label htmlFor="email">Email</label>
                <small style={{ padding: "3px", fontSize: "10px" }}>
                  Ex. email@email.com
                </small>
              </div>
              <div className={commonInputClass}>
                <input
                  value={formInput.password}
                  onChange={handleInputChange}
                  placeholder="Password"
                  required
                  type="password"
                  id="password"
                  name="password"
                  className="form-control"
                />
                <label htmlFor="password">Password</label>
              </div>
              <div className={commonInputClass}>
                <input
                  value={formInput.full_name}
                  onChange={handleInputChange}
                  placeholder="Full Name"
                  required
                  type="text"
                  id="full_name"
                  name="full_name"
                  className="form-control"
                />
                <label htmlFor="full_name">Full Name</label>
              </div>
              <div className={commonInputClass}>
                <input
                  value={formInput.address}
                  onChange={handleInputChange}
                  placeholder="Address"
                  required
                  type="text"
                  id="address"
                  name="address"
                  className="form-control"
                />
                <label htmlFor="address">Address</label>
              </div>
              <div className={commonInputClass}>
                <input
                  placeholder="Format: 123-45-678"
                  value={formInput.phone_number}
                  onChange={handleInputChange}
                  required
                  type="tel"
                  name="phone_number"
                  id="phone_number"
                  pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}"
                  className="form-control"
                />
                <label htmlFor="phone_number">Phone Number</label>
                <small style={{ padding: "3px", fontSize: "10px" }}>
                  Ex. Format: 123-456-6789
                </small>
              </div>
              <div className={operatingClass}>
                <textarea
                  value={formInput.operating_hours}
                  onChange={handleInputChange}
                  placeholder="Operating Hours"
                  type="text"
                  id="operating_hours"
                  name="operating_hours"
                  className="form-control"
                />
                <label htmlFor="operating_hours">Operating Hours</label>
              </div>
              <div className={volunteerInputClass}>
                <input
                  value={formInput.dl_num}
                  onChange={handleInputChange}
                  placeholder="Driver License Number"
                  type="text"
                  id="dl_num"
                  name="dl_num"
                  className="form-control"
                />
                <label htmlFor="dl_num">Driver License Number</label>
              </div>
              <div className={volunteerInputClass}>
                <input
                  value={formInput.plate_num}
                  onChange={handleInputChange}
                  placeholder="License Plate Number"
                  type="text"
                  id="plate_num"
                  name="plate_num"
                  className="form-control"
                />
                <label htmlFor="plate_num">License Plate Number</label>
              </div>
              <div className={providerInputClass}>
                <input
                  value={formInput.pickup_instructions}
                  onChange={handleInputChange}
                  placeholder="Pickup Instructions"
                  type="text"
                  id="pickup_instructions"
                  name="pickup_instructions"
                  className="form-control"
                />
                <label htmlFor="pickup_instructions">Pickup Instructions</label>
              </div>
              <div className={receiverInputClass}>
                <input
                  value={formInput.dropoff_instructions}
                  onChange={handleInputChange}
                  placeholder="Dropoff Instructions"
                  type="text"
                  id="dropoff_instructions"
                  name="dropoff_instructions"
                  className="form-control"
                />
                <label htmlFor="dropoff_instructions">
                  Dropoff Instructions
                </label>
              </div>
              <div className="text-end">
                <button className="btn btn-outline-dark">SIGN UP</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}
