import { NavLink } from "react-router-dom";
import { useGetTokenQuery, useLogOutMutation } from "./store/userApi";
import { useNavigate } from "react-router-dom";

function Nav() {
  const navigate = useNavigate();
  const [logOut] = useLogOutMutation();
  const { data: userData } = useGetTokenQuery();

  function handleLogout() {
    logOut()
      .unwrap()
      .then((payload) => {
        navigate("/");
      })
      .catch((error) => console.error("rejected", error));
  }

  function isLoggedIn() {
    if (userData) {
      if (userData.account.role === "provider") {
        return (
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item ms-2">
                <NavLink className="nav-link" to="/dashboard">
                  Dashboard
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink
                  className="nav-link"
                  aria-current="page"
                  to="/donations/history"
                >
                  History
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/donations/new">
                  Create New Donation
                </NavLink>
              </li>
            </ul>
            <button
              type="button"
              className="btn btn-outline-light me-2"
              data-bs-toggle="modal"
              data-bs-target="#staticBackdrop"
            >
              Logout
            </button>
          </div>
        );
      } else if (userData.account.role === "receiver") {
        return (
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item ms-2">
                <NavLink className="nav-link" to="/dashboard">
                  Dashboard
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink
                  className="nav-link"
                  aria-current="page"
                  to="/donations/history"
                >
                  History
                </NavLink>
              </li>
            </ul>
            <button
              type="button"
              className="btn btn-outline-light me-2"
              data-bs-toggle="modal"
              data-bs-target="#staticBackdrop"
            >
              Logout
            </button>
          </div>
        );
      } else {
        return (
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item ms-2">
                <NavLink className="nav-link" to="/dashboard">
                  Dashboard
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink
                  className="nav-link"
                  aria-current="page"
                  to="/donations/history"
                >
                  History
                </NavLink>
              </li>
            </ul>
            <button
              type="button"
              className="btn btn-outline-light me-2"
              data-bs-toggle="modal"
              data-bs-target="#staticBackdrop"
            >
              Logout
            </button>
          </div>
        );
      }
    } else {
      return (
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item ms-2">
              <NavLink className="nav-link" aria-current="page" to="/">
                Home
              </NavLink>
            </li>
          </ul>
          <button
            type="button"
            data-bs-dismiss="modal"
            onClick={() => {
              navigate("/login");
            }}
            className="btn btn-outline-light me-2"
          >
            Login
          </button>
          <button
            type="button"
            data-bs-dismiss="modal"
            onClick={() => {
              navigate("/signup");
            }}
            className="btn btn-outline-light me-2"
          >
            Sign up!
          </button>
        </div>
      );
    }
  }

  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="container-fluid">
        <div
          style={{ backgroundColor: "#f8f8f2", width: "7rem" }}
          className="p-1 rounded text-dark fw-bold text-center fs-4"
        >
          Foodwill
        </div>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        {isLoggedIn()}

        <div
          className="modal fade"
          id="staticBackdrop"
          data-bs-backdrop="static"
          data-bs-keyboard="false"
          tabIndex="-1"
          aria-labelledby="staticBackdropLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h1 className="modal-title fs-5" id="staticBackdropLabel">
                  Logging out
                </h1>
                <button
                  type="button"
                  className="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                ></button>
              </div>
              <div className="modal-body">
                <h5>Oh no! You're leaving... Are you sure?</h5>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-bs-dismiss="modal"
                >
                  Nah, just kidding
                </button>
                <button
                  type="button"
                  data-bs-dismiss="modal"
                  onClick={handleLogout}
                  className="btn btn-primary"
                >
                  Yes, log me out
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
