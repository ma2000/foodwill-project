import React from "react";
import { useState } from "react";
import { useCreateDonationMutation } from "./store/donationApi";
import { useNavigate } from "react-router-dom";
import { useGetTokenQuery } from "./store/userApi";

export default function DonationNew() {
  const navigate = useNavigate();
  const [createDonation] = useCreateDonationMutation();
  const { data: userData } = useGetTokenQuery();
  const [formInput, setFormInput] = useState({
    quantity: "",
    value: "",
    ready_time: "",
    status: "UNCLAIMED",
  });

  const handleInputChange = (e) => {
    setFormInput({
      ...formInput,
      [e.target.id]: e.target.value,
    });
  };

  const clearState = () => {
    setFormInput({
      quantity: "",
      value: "",
      ready_time: "",
      status: "UNCLAIMED",
    });
  };

  const handleFormSubmit = async (e) => {
    e.preventDefault();
    let data = { ...formInput };
    data["provider_id"] = userData.account.id;
    data["prov_name"] = userData.account.full_name;
    data["prov_address"] = userData.account.address;
    data["prov_phone"] = userData.account.phone_number;
    data["pickup_instructions"] = userData.account.pickup_instructions;
    data["active"] = true;
    createDonation(data)
      .unwrap()
      .then((payload) => {
        navigate("/dashboard");
      })
      .catch((error) => console.error("rejected", error));
    clearState();
  };

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <div className="text-center mb-4">
              <h1>Create a new donation</h1>
            </div>
            <form onSubmit={handleFormSubmit} id="create-donation-form">
              <div className="form-floating mb-3">
                <input
                  value={formInput.quantity}
                  onChange={handleInputChange}
                  placeholder="Quantity"
                  required
                  min={0}
                  max={1000}
                  type="number"
                  id="quantity"
                  name="quantity"
                  className="form-control"
                />
                <label htmlFor="quantity">Quantity (lbs) </label>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={formInput.value}
                  onChange={handleInputChange}
                  placeholder="Value"
                  required
                  min={0}
                  max={10000}
                  type="number"
                  id="value"
                  name="value"
                  className="form-control"
                />
                <label htmlFor="value">Value ($)</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={formInput.ready_time}
                  onChange={handleInputChange}
                  placeholder="ready time"
                  required
                  type="datetime-local"
                  id="ready_time"
                  name="ready_time"
                  className="form-control"
                />
                <label htmlFor="ready_time">Ready Time</label>
              </div>
              <div className="text-end">
                <button className="btn btn-outline-dark">CREATE</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}
