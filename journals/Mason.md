
################################

11/15/22
-Made API documentation as group
-Continued documentation, updated diagram
-Discussed & agreed as a group to make "donations/orders" it's own microservice
-Made API endpoints as a group

11/16/22
-Agreed to put status codes to denote order life cycle phase
-Did more API endpoint documentation as group
-Added MVP explanation
-Changed user into 1 microservice and 1 login page, whereas before it was 3 microservices with 3 login pages
-Adding roles property to user login microservice


11/17/22
-Built Docker-compose.yaml file up and working
-Requirements.txt files updated
-Docker containers built and working according to docker-compose.yaml file

11/18/22
-Did CRUD, endpoints of donations
-More fixes on docker yaml file
-Worked on project, took turns driving/navigating for pair programming and making commits, approving commits

11/21/22
-Got MongoDB up and running, fixed mongodb issues inside docker yaml file
-Got Mongo Express working, configured docker yaml file for Mongo Express
-Made some routers for donations as group
-De-bugged several issues
-Made edits to quieries as group

11/22/22:
-Made put, delete for donations as team
-Drive/Navigate took turns
-De-bugged some crucial issues like _id vs id, and ObjectId
-Started adding default status to post requests
-Learned about ObjectId, _id, id meanings in MongoDB and pymongo


11/23/22:
-Updated all donations so status codes could work
-Add status code system to DontionQuery, get_all_donations router
-Condensed all status codes into 1 api endpoint
-Learned how to put in status codes into lists, filter by status codes, and learned how to condense everything into 1 endpoint, and using urls with fastapi

-Added put requests to change status of donations from "unclaimed" to "enroute" and from "enroute" to "claimed"

11/28
-Started React pages for receiver. Did React receiver form- working with phone, email,address, submit.
-Learned how to make nice looking react input form for contact information. Rounded shape buttons with nice colors. Later removed colors b/c we're doing it plain so can use bootstrap later

11/29
-Did receiver form in React. Also started receiver donation list form.
-Made 2 versions of receiver form to try 2 different methods. 1 looked better, 1 had better functionality
-Discussed tony's login system as group. Group approved updates and login system. Discussed possible improvements and effects on other software compenents, and how will interact
-Learned more about react, learned more about login systems using mongodb and with tokens. Learned about react forms


11/30
-Discussed as group updates to react forms to take into account tokens.
-Discussed as group how to make react forms auto-populate certain user info
-Made adjustments to react forms based on group discussions from earlier today
-Learned how to make improvements to react forms which make them more robust, and make them work with a token login system instead of based on id
 

12/1
-Got receiver section 100% working, backend and front end
-Got all react components 100% working
-Added filtering mechanism for receiver to only see their unclaimed donations
-Add button to refresh seeing new donations added
-Added nav bar for receiver so receiver can navigate between only receiver pages.
-Learned about filering mechanisms on react and how to filter from front end instead of just bacend
-Learned how to add refresh button


12/2
- Worked with team to figure out refactoring all of app using redux
-Started working on various components with team on refactoring with redux, and making templates for redux components
-Started refactoring receiver using redux
-Learned about redux and how it effects automatically updating page with new data, without having to use refresh


12/5
-More redux with team, refactoring components and making templates for other components
-Worked on several components on app with team to refactor, and make improvements to refactoring
-Learned about refactoring and what translates over in refactoring, and what doesn't



12/6
-Refactored receiver update form again with improvements we made as team. Refactoring list page as well
-Found bug in update form page for all components of app, that updated info still had old info
-Collaborated with team to fix bug, redo update form page, log in functionality for all of app. Agreed as team to have solution be 
-Added styling with team, using bootstrap
-Learned more about redux, learned about pinpointing bugs

12/7
-Redid routes of all files for app as team
-Refactored volunteer, provider, receiver as team. Added dashboards for each with components inside components. Made templates for receiver, provider, volunteer
-Added more lists for all components
-Learned more about routing

12/8
-Did deployment. Did Mongo Atlas
-Updated code to handle edge cases, improved dashboards, improved route, updated donations to not allow negative numbers and allow decimal numbers and have a max size.
-Learned about deployment and how to do it. Learned about Mongo Atlas


12/9
-Did unit testing as team, as well as individual unit tests
-Finished deployment last night as team. Deployment 100% successful
-Unit testing 100% successful
-Learned about unit testing, pytest and testing api endpoints

