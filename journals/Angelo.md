### 12-09-2022
Today I worked on updating our readme file and wrote a unit test. No major blockers or issues, hopefully when we deploy here shortly all will be well. If there are no issues then this will be my last journal entry for the project (fingers crossed).

### 12-08-2022
We reconvened and worked on CI/CD all day. In the morning I drove while we quad-programmed and setup our mongo database and our render account. In the afternoon Tony drove while we deployed our frontend to gitlab and our api layers to render. We had issues for some time as render had a major outage on the server we were deploying to. Once we had our first successful deployment to render there was an issue with a token perpetually being fetched, and with the help of James eventually realized we hadn't changed the names of our environment variables in our gitlab yml file to reflect those we declared in render. After we fixed that bug and deployment successful we attended to some other minor issues.

### 12-07-2022
I worked with Mason and Trewin in getting their components and dashboards updated with the latest redux refactoring and changes to the donation models. After updating all user components we quad-programmed with Tony and implemented routing and navigation site-wide. The rest of the evening I will be working on CI/CD.


### 12-06-2022
Today we reflected on the structure and functionality of our application and decided we needed to add more properties to our donation model so each type of user would have all the relevant information necessary in their dashboard components. This took a little bit of refactoring but was relatively easy to complete.
Afterward Tony, Mason and I pair-programmed a dashboard template for everyone to use. He came up with the idea to create a main file for each type of user, pass the relative components into that main component and then pass the main component into a dashboard component. This strategy required a considerable amount of refactoring and changing the way user data was passed into components. Once the dashboard was functional we worked a bit on styling it as well as the donation detail card.

### 12-05-2022
Today I worked on the donation detail component, a test user donations list, and made my volunteer donation list components, implementing RTK in each. RTK presents a number of issues when implementing but once the bugs are resolved it works like a charm.

### 12-02-2022
Today we pair-programmed, I drove while Tony and Mason navigated, and implemented Tony's RTK changes to the user service on the volunteer update information form. We discovered we need to call in the users data when the app first loads to have it available to auto-populate the form with their existing information, otherwise if we called it from the form component it wasn't loading quickly enough. After completing the RTK implement I walked Mason through the same for the receiver update information form. I then worked on a donation detail component for the rest of the evening, this required utilization of useParams from 'react-router-dom' and creating another query in the donationApi slice.

### 12-01-2022
I spent last night and this morning/afternoon working on a donations list that allows a user to modify the status of a donation with a button click via RTK query. I was able to get the gets to work but not the puts, I walked Tony through my code and a little while later he came up with a solution. i was trying to pass in two arguments to the mutation but should've only sent in one, and i didn't call the useMutation properly on the donations list component. I'll spend the rest of tonight working on completing the donations list and adding get queries to filter volunteer-specific donation data. 

### 11-30-2022
Tony walked me through the process of accessing the backend of the user service to complete the functionality on my volunteer update form; we implemented a useEffect hook in the main app file to access the users' token from the get-go. Tony guided me through getting the form to autofill the volunteers current information so they could see what they are updating. After some code cleaning I resent merge request to Trewin for approval. We decided we would all work with redux for the remainder of the afternoon to see if it was worth implementing in our donation service.

### 11-29-2022
Today I talked with Tony before lunch about the reusability of some components' code across our frontend, our users' interface and functionality is dependent on their role and some functionality is similar across roles. Through our discussion we mapped component reusability and Tony added this to our wireframe as well as file names for each aspect of our users' UI. After lunch/lecture Tony walked us through the user service and user authentication he completed last night. We discussed how the frontend will interact with the backend (in theory) and decided we would work together on at least one of the more complex components that all of our users will interact with. I am working on the volunteer UI and building a form for updating their information and starting on a component that will allow them to see all the donations available for delivery. I completed the volunteer update form but need to get together with Tony about posting to the backend because I am unable to get use effect to load a user token.
 

### 11-28-2022
Today we regrouped after the long break and discussed direction going forward. We still have to complete the user service backend which includes user authentication and authorization, Tony said he worked on it a little over the break and would finish it tonight. We divided up some of the frontend between Mason, Trewin and I and worked individually the latter part of the afternoon into the evening. I am using hooks for the first time and so I was researching/attempting to call to the backend using useEffect and setting state with a simple get all donations hook. I ran into an issue not being able to access the data and will address it with the team tomorrow for some assistance/more eyes on the code. I have some questions regarding how we will handle state across components and will address those with the team tomorrow, I think redux toolkit might make it easier but perhaps our app is not that complex and passing through props will suffice.

### 11-22-2022
Tony suggested some changes to the merge request I sent to him yesterday, he thought it was unnecessary to create another pydantic model to resolve the ObjectId issue and it was, just needed to convert the result id back to a string. Next, we wrote the update operation. There was an issue utilizing the pymongo update_one method but we resolved it by referencing the documentation and passing in the filter and update arguments as dictionaries/objects. We finished out the CRUD operations by adding the delete operation. We started looking at how we will use the Put operation to change its status property but did not get it to work, will get back to it tomorrow.


### 11-21-2022
Today I drove while we worked as a group on CRUD operations for our donation microservice; creating a donation and getting all donations went pretty smoothly. We then pivoted to setting up Mongo Express for optimal interface with our database, Tony drove. We continued with CRUD and ran into a blocker when attempting to get an individual donation, we were getting a null value returned even though the response was 200 okay. We dug into the pymongo literature and discovered we could use bson.objectid to convert the donation id property from a string to an ObjectId so it could be passed to the find_one method and we get a response body. I implemented the change and sent to Tony for approval. 

### 11-18-2022
Built out yaml file and created services together as a group. Walked through gitlab merge process including merge requests as a group. I worked on editing the MVP and started the readme file while Trewin and Mason started working on the endpoints.

### 11-17-2022
Met with James for review of endpoints, received ok. Group discussed next steps, will build out services and start on endpoints tomorrow.

### 11-16-2022
Worked on endpoints design briefly in api-design file. Met briefly with James, advised on microservices structure, he will check back tomorrow. 

#### 11-15-2022
Today we discovered the API design file we worked on last Thursday in the Gitlab ide did not save properly. I recreated the file and put in the endpoints for donations and provider microservices, Mason will complete the other two tonight and we will review with the whole team tomorrow.

We discussed modeling and everyone is in agreement that Donations should be its own microservice. We will further contemplate the other microservices when Tony is back from absence.
