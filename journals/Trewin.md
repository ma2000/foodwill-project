### 12-8-2022
Worked on unit tests and wrapped up. Lets goo

Worked on CD/CI and finally got it working. Render is terrible. Pay to win service and we eventually one. Really impressed with the whole group

### 12-7-2022
Finished all routing and did some minor bug fixes. Created dashboards for all user types and created conditional rendering for all of them. Did some minor functionality tweaks and visual tweaks.

### 12-6-2022
Finished the all the neccessary components for the providers and started prepping to work on the dashboard stuff. Hopefully all will translate nicely to the dashboard but may need to refactor

### 12-5-2022
Felt better returning from weekend and got a lot of stuff done during it. Decided to convert everything to hooks and will start implementing redux as well. Auth shouldn't be a problem anymore and I am feeling optimistic about deployment. Need to start thinking about unit testing

### 12-1-2022
Felt like I got nothing done today. Not feeling good, very fuzzy. Left before last attendance.

### 11-30-2022
Continued working on my components, not feeling very confident about class based components and their functionality with author no do I feel confident about hooks

### 11-29-2022
Discussed user auth/working together for some of the more complicated parts of the app. Research redux and considering whether to use hooks or class based components, far more comfortable with class components but it seems everyone want to use hooks. Also redux continues to be dicussed as a way to hold the user info for the session.

### 11-28-2022
Got back together today and I felt like I had already forgotten everything. Still need to do some user backend stuff and I do not fully understand auth yet but Tony is very confident and that he had worked on it over break. We divided up the frontend and I will be working on the Restaurant portion of the components. Need to make some decisions on how I should  build these components and there has been some discussion of Redux. I don't feel confident at all about Redux but I trust the group.

### 11-22-2022
Thanksgiving, had to leave early do to flight changes. Hope group is okay, getting many message notifications and having trouble following it all without context

### 11-21-2022
Worked on donations microservice and implemented Mongo Express. Seemed superflous at first but ended up really helping. Accomplished a lot with the CRUD operations

### 11-18-2022
Worked on queries with Mason and ran into some issues while Angelo worked on MVP in README. Working with FASTAPI is foreign and hard. Additionally, never had to consider how they connected with the db and what yaml work we needed to do to make that happen.

### 11-17-2022
Endpoints given the okay from James, will continue to work on them

### 11-16-2022
Worked on endpoints and met with James about them.

#### 11-15-2022
Gitlab pipes were not working so we lost some work that Angelo redid: our endpoints. Need to confirm that won't happen again. Discussing how microservices should exist and that donations should be its own service.
