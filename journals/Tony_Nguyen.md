### 11-16-2022

Finishing up endpoint designs but then James suggested that we should make Receivers, Providers, Volunteer microservices into one User microservice so we probably need to revise it tomorrow.

### 11-17-2022

Revised microservices design as a group. From now, we only have 2 micros: Donations and Users. James reviewed and approved. Group will start building actual endpoints tomorrow.

### 11-21-2022

Group programming. Angelo and I were the drivers for the day. We set up MongoExpress and started on CRUD for Donations micro. We ran into a minor blocker with trying to retrieve a single donation by its ID. Solved it by using ObjectId() to convert the regular input str into the actual ObjectId. We will probably finishing up the rest of Donations's CRUD tomorrow.

### 11-22-2022

Group programming. We worked on PUT and DELETE for Donations micro. We ran into another minor blocker with the syntax of update_one function of Pymongo but we figured it out after a bit. We also cleaned up unused codes and made everything nice and neat.

Solo programming. I updated Donations's PUT method so it can be called to update just one or as many fields as needed (like updating only the status field of a Donation) by using exclude_unset with data.dict(). I changed Donation's POST method so everytime a Donation got created, it will automatically have status set as "UNCLAIMED".

### 11-28-2022

Solo Programming. Today we met and talk about what Mason worked on last Wednesday, cleaned up the codes and approved and merged to main branch. Then we decided that we should divide the work loads and start solo programming to get things done quicker.

My task was to build the User Microservice and implement User Authenticator with it. I took a deep dive into research about User Auth for a few hours then started coding. I ran into a small blocker which was managing which Model to use for authenticators in and out. After debugging step by step and carefully read the errors in the logs, I was able to understand how everything works and finish the Users Mircroservice. I sent a merge request to Angelo for review. I will explain what I did in team's stand up tomorrow.

### 11-29-2022

Today we solo programming. I gotta refactor the backend User Auth because what I did yesterday introduced some bugs. Yesterday's work only works when we use the FastApi admin page to access endpoints. So basically I only finished Auth for Backend.

So my task of the day was to figure out Frontend Auth. I ran into a blocker that eventhough I logged in using the FastApi admin page, I still can't fetch from Frontend. I think I need to figure out a way to send the fastapi_token along with every fetch I want to make.

End Of Day status: still trying to figure out how to send token with fetch.

### 11-30-2022

Today we solo programming. I finally got through the blocker I have yesterday, by adding [credentials: "include"] to every fetch I made.

Technically, Frontend Auth is working now. However, the team is trying to decide if we want to use RTK to manage state or we should stick with tradition React state managing. So I will dive into Redux for the rest of the day to see if this is something we need for our Application.

End Of Day status: still researching and try to understand Redux, but I think I'm close. I think Redux will be very usefull moving forward.

### 12-01-2022

Today we solo programming. I learned about what pros and cons of RTK from yesterdat researching. I believe RTK will help ease our jobs a lot, especially with the Donation Service. RTK will take care of managing and keeping the state with the most updated data for Donations for us so we don't have to implement a poller or constantly sending Get Request to the BackEnd to get fresh data. I shared my understanding with the team and we decided to implement it in our App.

So my task for today is refactoring our codes so we can use RTK to managing fetches and states for both Services.

End Of Day status: finished today task, finished Login Page, implemented RTK for both Donation and User Service, Authentication is now required to access all services, Sending merge request to Angelo for review.

### 12-02-2022

Today we pair programming. Angelo drives while I navigates. I show Angelo and Mason how to implement the new RTK update to their codes. Then refactor Angelo component and figure out a way to pre-populated the form with default data. We ran into a small issue where we try to access the data before the data got back from the query, which will cause the app to break. We solved it by utilizing isLoading/isFetching before passing the data in.

My task for the day is to work on the Sign Up page. So I did that for the rest of the evening.

End Of Day status: still working on Sign Up page.

### 12-05-2022

Today we group programming. Angelo drives while Mason, Trewin and I navigates. We finished up DonationDetail Page and wrote a few more Query and to implement GetActiveDonationByUser and GetDonationByUser. We added the active field to the Donation Model to make it easier to filter. We brought Mason and Trewin up to speed with all the new changes then we broke up and each worked on their own tasks.

My task for the day is the finish up the Sign Up Page then ClaimedList and UserDonationsList.

End Of Day status: TBD.


### 12-06-2022

Today we pair programming. We discussed as a group that the Donation Service will need a bit more function and more fields so we added that and refactored the Service accordingly. Then we spent the rest of the day working on Volunteer Dashboard Component and all little components inside it and then we styled it a bit for more user friendly interface.

### 12-07-2022

Today we pair programming. Yesterday, we already finished up Volunteer Dashboard Component so we can use that as a template for Provider and Receiver. Mason and Angelo working on the Receiver Dashboard and all the component inside while Trewin and I working on the Provider Dashboard. In the evening, we regrouped and work on Routing and cleaned up unused codes.

### 12-08-2022

Today we pair programming. We worked on Deployment all day today. Render was having issue on their ends all day so testing deployment took really long. We ran into a issue where after we deployed the app, it got stuck in a infinite loop of calling request to /token. It was caused by a mismatch of the variable name of the API Services URL in Gitlab yml and Docker-compose yml file. That variable name should be the same across all files.

End Of Day status: The app is deployed successfully, up and running with no error or warnings in the console.

### 12-09-2022

Today we solo programming. We got everything to work and deployed yesterday, so today we gona work on writing our own unit test.

End Of Day status: I finished writing my unit test and pushed and merged to main branch. Pipeline passed and successfully deployed..
